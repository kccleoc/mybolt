// test based on this post idea
// https://groups.google.com/forum/#!topic/google-appengine-go/Kh1eLUROq90
package mybolt

import (
	"encoding/gob"
	"testing"

	"github.com/sirupsen/logrus"
)

type VanityKey struct {
	Prefix []byte
	K      []byte
}

var (
	// test data
	Key  = []byte("btc")
	vkey = VanityKey{
		Prefix: []byte("1Leo"),
		K:      []byte("1d3455ef194895845804938501"),
	}

	// db info
	bucketName = []byte("bucket-main")
	dbLocation = "1.db"
)

func init() {
	// x is the things going to be in and out from db
	// must be register in beginning for decoded interface to
	// reflect the value to a specific struct, otherwise
	// nil return
	x := &VanityKey{}
	gob.Register(x)
}

func TestPut(t *testing.T) {

	buck, err := OpenBucket(bucketName, dbLocation)
	if err != nil {
		logrus.Errorf("PUT: %s", err)
	}
	defer buck.Close()

	if err = PutItem(buck, Key, vkey); err != nil {
		logrus.Error(err)
	}
}

func TestGet(t *testing.T) {

	buck, err := OpenBucket(bucketName, dbLocation)
	if err != nil {
		logrus.Errorf("GET: %s", err)
	}
	defer buck.Close()

	b := &VanityKey{}

	if err := GetItem(buck, Key, b); err != nil {
		return
	}

	logrus.Infof("SUCCESS! Item get:\nKey: %s|Value: %s", Key, b)

}

func TestList(t *testing.T) {
	buck, err := OpenBucket(bucketName, dbLocation)
	if err != nil {
		logrus.Errorf("LIST: %s", err)
	}
	defer buck.Close()

	ListItem(buck)
}

func TestDelete(t *testing.T) {
	buck, err := OpenBucket(bucketName, dbLocation)
	if err != nil {
		logrus.Errorf("DEL: %s", err)
	}
	defer buck.Close()

	DelItem(buck, []byte("btc8"))
}
