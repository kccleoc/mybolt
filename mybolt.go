package mybolt

import (
	"bytes"
	"encoding/gob"
	"errors"
	"fmt"
	"time"

	"github.com/boltdb/bolt"
	"github.com/sirupsen/logrus"
)

// Bucket is boltdb buckets
type Bucket struct {
	DB   *bolt.DB
	Name []byte
}

// Item is the k v pair to be put into boltdb
type Item struct {
	Key, Value []byte
}

func (bk *Bucket) Close() {
	bk.DB.Close()
	logrus.Debugf("Bucket %s closed", bk.Name)
}

// OpenBucket creates and opens a named bucket.
// There is no defer close in this func
func OpenBucket(bucketName []byte, path string) (*Bucket, error) {

	// open DB
	config := &bolt.Options{Timeout: 1 * time.Second}

	db, err := bolt.Open(path, 0600, config)
	if err != nil {
		return nil, err
	}

	// create bucket
	if err := db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists(bucketName)
		if err != nil {
			logrus.Error("could not create bucket during DB update")
			return err
		}
		return nil
	}); err != nil {
		return nil, err
	}

	return &Bucket{db, bucketName}, nil
}

// PutItem puts gob encrypted k v into bucket
func PutItem(bk *Bucket, key []byte, in interface{}) error {
	// Encode the entry for storing.
	var buf []byte
	b := bytes.NewBuffer(buf)
	enc := gob.NewEncoder(b)
	err := enc.Encode(in)

	// If there was an encoding failure, log it and return it.
	if err != nil {
		logrus.Errorf("Error encoding data for cache (%s): %v", key, err)
		return err
	}

	// Create boltdb.Item for storing.
	item := &Item{
		Key:   key,
		Value: b.Bytes(),
	}

	// put to bucket
	if err := bk.put(item); err != nil {
		return err
	}
	logrus.Infof("SUCCESS! Item %s added ", item.Key)
	return nil
}

// put inserts value `v` with key `k`
func (bk *Bucket) put(i *Item) error {

	return bk.DB.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(bk.Name)
		idx, _ := b.NextSequence()
		i.Key = autoKeySuffix(i.Key, idx)
		return b.Put(i.Key, i.Value)
	})
}

func autoKeySuffix(k []byte, s uint64) []byte {
	suffix := fmt.Sprintf("%x", s)
	logrus.Debugf("%v", suffix)
	return append(k, []byte(suffix)...)
}

// GetItem retrieves the value for key `k`
func GetItem(bk *Bucket, key []byte, out interface{}) error {

	// get item first
	e, err := bk.get(key)

	// log when missed a record
	if err != nil {
		logrus.Error(err)
		return err
	}

	r := bytes.NewReader(e)
	dec := gob.NewDecoder(r)
	return dec.Decode(out)
}

// get will return nil if nothing get
func (bk *Bucket) get(k []byte) ([]byte, error) {
	value := []byte{}
	if err := bk.DB.View(func(tx *bolt.Tx) error {
		v := tx.Bucket(bk.Name).Get(k)
		if v != nil {
			value = make([]byte, len(v))
			copy(value, v)
			return nil
		}
		return errors.New("could not get record, key:" + string(k))
	}); err != nil {
		return nil, err
	}
	return value, nil
}

func ListItem(bk *Bucket) {
	if err := bk.DB.View(func(tx *bolt.Tx) error {
		// Assume bucket exists and has keys
		b := tx.Bucket(bk.Name)

		c := b.Cursor()

		for k, v := c.First(); k != nil; k, v = c.Next() {
			fmt.Printf("%s, %s\n----------\n", k, v)
		}
		return nil
	}); err != nil {
		logrus.Error("could not list items in Bucket %s\n", bk.Name)
	}
}

func DelItem(bk *Bucket, key []byte) {
	if err := bk.del(key); err != nil {
		logrus.Error(err)
		return
	}
	logrus.Infof("SUCCESS! Item %s deleted ", key)
}

func (bk *Bucket) del(k []byte) error {
	return bk.DB.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(bk.Name)
		return b.Delete(k)
	})
}
